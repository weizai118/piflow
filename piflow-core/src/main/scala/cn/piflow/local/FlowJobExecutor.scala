package cn.piflow.local

import cn.piflow._
import cn.piflow.util.Logging
import com.google.common.graph.{MutableValueGraph, ValueGraphBuilder}
import org.apache.spark.sql.{SQLContext, SparkSession}
import org.quartz.JobExecutionContext

import scala.collection.JavaConversions.asScalaSet
import scala.reflect.ClassTag

class FlowJobExecutionException(nodeId: Integer, proccesor: Processor, cause: Throwable)
	extends FlowException(s"fail to execute job, nodeId=$nodeId, processor=$proccesor", cause) {
}

class AnalyzedFlowGraph(flowGraph: FlowGraph) {
	val nodesMap = collection.mutable.Map[Int, FlowNode]();
	val graph: MutableValueGraph[Integer, (String, String)] =
		ValueGraphBuilder.directed().build();

	flowGraph.nodes.foreach { node =>
		graph.addNode(node.id);
		nodesMap(node.id) = node;
	}

	flowGraph.links.foreach { link =>
		graph.putEdgeValue(link.source.id, link.target.id, link.portNamePair);
	}

	def node(id: Int) = nodesMap(id);
}

class FlowJobExecutor(jobManager: FlowJobServiceImpl) extends Logging {
	lazy val spark = SparkSession.builder.getOrCreate();

	def executeFlowGraph(flowGraph: FlowGraph, jec: JobExecutionContext) {

		val ctx: JobContext = new JobContext() {

			val map = collection.mutable.Map[String, Any](
				"checkpointLocation" -> spark.conf.get("spark.sql.streaming.checkpointLocation")
			);

			def notifyEvent(event: FlowEvent) = jobManager.receive(jobInstanceId, event);

			def sparkSession(): SparkSession = spark;

			def sqlContext(): SQLContext = spark.sqlContext;

			def flowGraph(): FlowGraph = flowGraph;

			def jobInstanceId: String = jec.getFireInstanceId;


			def arg[T](name: String): T = map(name).asInstanceOf[T];

			def arg[T](name: String, value: T) = {
				map(name) = value;
				this;
			}

			def notify(message: FlowEvent) = {
				//TODO: async
				jobManager.receive(jobInstanceId, message);
			}
		}

		//TODO: analyze flow graph
		val graph = new AnalyzedFlowGraph(flowGraph);
		//initializing all nodes first
		for (flowNode <- flowGraph.nodes) {
			val pctx = new ProcessorContext {
				override def arg[T](name: String) = ctx.arg(name);

				override def notifyEvent(event: FlowEvent) = ctx.notifyEvent(event);

				override def sparkSession = ctx.sparkSession;

				override def sqlContext: SQLContext = ctx.sqlContext;

				override def argForType[T: ClassTag](implicit m: Manifest[T]): T = ctx.argForType[T];

				override def arg[T](name: String, value: T) = {
					ctx.arg(name, value);
					this;
				}

				override def flowGraph: FlowGraph = ctx.flowGraph;

				override def jobInstanceId: String = ctx.jobInstanceId;

				def jobContext: JobContext = ctx;

				def flowNodeId: Int = flowNode.id;
			};

			val processor = flowNode.processor;
			processor.init(pctx);
		};

		val afg = new AnalyzedFlowGraph(flowGraph);
		val visitedNodes = scala.collection.mutable.Map[Integer, Map[String, Any]]();
		val endNodes = afg.graph.nodes().filter(afg.graph.successors(_).isEmpty());
		logger.debug(s"end nodes: $endNodes");
		endNodes.toSeq.foreach(visitNode(afg, _, visitedNodes, ctx));
	}

	private def visitNode(afg: AnalyzedFlowGraph, nodeId: Integer,
	                      visitedNodes: scala.collection.mutable.Map[Integer, Map[String, _]],
	                      ctx: JobContext): Map[String, _] = {
		if (visitedNodes.contains(nodeId)) {
			visitedNodes(nodeId);
		}
		else {
			val thisNode = afg.node(nodeId);
			val inputs = collection.mutable.Map[String, Any]();

			//predecessors
			val predecessorNodeIds = afg.graph.predecessors(nodeId);
			for (predecessorNodeId ← predecessorNodeIds) {
				val edgeValue = afg.graph.edgeValue(predecessorNodeId, nodeId).get;
				val outputs = visitNode(afg, predecessorNodeId, visitedNodes, ctx);
				if (edgeValue._1 != null && edgeValue._2 != null) {
					inputs += (edgeValue._2 -> outputs(edgeValue._1));
				}
			}

			val processor = thisNode.processor;
			logger.debug(s"visiting node: $processor, node id: $nodeId");

			try {
				processor.notifyEvent(ProcessorStarted());
				val outputs = processor.performN2N(inputs.toMap);
				processor.notifyEvent(ProcessorCompleted());
				logger.debug(s"visited node: $processor, node id: $nodeId");
				visitedNodes(nodeId) = outputs;
				outputs;
			}
			catch {
				case e: Throwable => {
					processor.notifyEvent(ProcessorFailed());
					throw new FlowJobExecutionException(nodeId, processor, e);
				}
			}
		}
	}
}